
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Eszter
 */
public class TicTacToeGame implements Board{
    
    private Cell[][] board = new Cell[3][3];

    public Cell[][] getBoard() {
        return board;
    }
    
    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        Cell actCell = new Cell(rowIdx, colIdx);
        rowIdx = actCell.getRow();
        colIdx = actCell.getCol();
        if ( rowIdx != 0 && rowIdx != 1 && rowIdx != 2 ) {
            String message = "Row index ("+rowIdx+") out of board!";
            throw new CellException(rowIdx, colIdx, message);
        }
        if ( colIdx != 0 && colIdx != 1 && colIdx != 2 ) {
            String message = "Column index ("+colIdx+") out of board!";
            throw new CellException(rowIdx, colIdx, message);
        }
        return actCell.getCellsPlayer();
    }
    

    @Override
    public void put(Cell cell) throws CellException {
        int rowIdx = cell.getRow();
        int colIdx = cell.getCol();
        if (rowIdx != 0 && rowIdx != 1 && rowIdx != 2 && colIdx != 0 && colIdx != 1 && colIdx != 2) {
            String message = "Index out of board!";
            throw new CellException(cell.getRow(), cell.getCol(), message);
        }
        if (cell.getCellsPlayer() != PlayerType.EMPTY) {
            String message = "Cell is already filled!";
            throw new CellException(cell.getRow(), cell.getCol(), message);
        }
        PlayerType actPT = cell.getCellsPlayer();
    }
    

    @Override
    public boolean hasWon(PlayerType p) {
        boolean result = false;
        fillBoard();
        int rowIdx;
        int colIdx;
        for (rowIdx = 0 ; rowIdx <= 2 ; rowIdx++ ) {
            if (board[rowIdx][0].getCellsPlayer() ==  board[rowIdx][1].getCellsPlayer()
                    && board[rowIdx][0].getCellsPlayer() ==  board[rowIdx][2].getCellsPlayer()) {
                result = true;
            }
        }
        for (colIdx = 0 ; colIdx <= 2 ; colIdx++ ) {
            if (board[0][colIdx].getCellsPlayer() ==  board[1][colIdx].getCellsPlayer()
                    && board[0][colIdx].getCellsPlayer() ==  board[2][colIdx].getCellsPlayer()) {
                result = true;
            }
        }
        if (board[0][0].getCellsPlayer() == board[1][1].getCellsPlayer() 
                && board[0][0].getCellsPlayer() == board[2][2].getCellsPlayer()) {
            result = true;
        }
        if (board[0][2].getCellsPlayer() == board[1][1].getCellsPlayer() 
                && board[1][1].getCellsPlayer() == board[2][0].getCellsPlayer()) {
            result = true;
        }
        return result;
    }
    
    
    public Cell[][] fillBoard () {
        int rowIdx;
        int colIdx;
        for (rowIdx = 0 ; rowIdx <= 2 ; rowIdx++ ) {
            for (colIdx = 0 ; colIdx <= 2 ; colIdx++ ) {
                board[rowIdx][colIdx] = new Cell(rowIdx, colIdx);
            }
        }
        return board;
    }
    

    @Override
    public List<Cell> emptyCells() {
        fillBoard();
        List<Cell> result = new ArrayList<>();
        int rowIdx;
        int colIdx;
        for (rowIdx = 0 ; rowIdx <= 2 ; rowIdx++ ) {
            for (colIdx = 0 ; colIdx <= 2 ; colIdx++ ) {
                if (board[rowIdx][colIdx].getCellsPlayer() == PlayerType.EMPTY) {
                    result.add(board[rowIdx][colIdx]);
                }
            }
        }
        return result;
    }
    
}
