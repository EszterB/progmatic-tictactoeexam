

package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;

/**
 *
 * @author Eszter
 */
public class SimplePlayer extends AbstractPlayer {

    public SimplePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        Cell result;
        int rowIdx;
        int colIdx;
        for (rowIdx = 0 ; rowIdx <= 2 ; rowIdx++ ) {
            for (colIdx = 0 ; colIdx <= 2 ; colIdx++ ) {
                if (b.getCell(rowIdx, colIdx) == PlayerType.EMPTY) {
                    //TODO
                }
            }
        }
    }
    
}
