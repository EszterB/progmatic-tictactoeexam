

package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;

/**
 *
 * @author Eszter
 */
public class VictoryAwarePlayer extends SimplePlayer{
    
    public VictoryAwarePlayer(PlayerType p) {
        super(p);
    }
    
}
